# centos-rust-mini

ARG DOCKER_REGISTRY_URL=registry.gitlab.com/docking/
ARG CUSTOM_VERSION=0.4
ARG CENTOS_VERSION=7
ARG RUST_VERSION=1.45.2


FROM ${DOCKER_REGISTRY_URL}centos:${CUSTOM_VERSION}-${CENTOS_VERSION} AS centos-rust-mini

ARG RUST_VERSION

WORKDIR /root

ENV RUSTUP_HOME=/usr/local/lib/rustup

RUN \
	curl --output ./rustup-init --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs ; \
	chmod a+x ./rustup-init ; \
	CARGO_HOME=/usr/local \
	./rustup-init -y --no-modify-path --default-toolchain ${RUST_VERSION} --profile minimal ; \
	rm ./rustup-init
